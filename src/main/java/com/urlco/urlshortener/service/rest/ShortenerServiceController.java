package com.urlco.urlshortener.service.rest;

import java.io.IOException;
import java.net.InetSocketAddress;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.urlco.urlshortener.dto.ShortenerBean;
import com.urlco.urlshortener.service.api.ShortenerService;

@RestController
public class ShortenerServiceController {

	private Log logger = LogFactory.getLog(ShortenerServiceController.class);
	
	@Autowired
	private ShortenerService shortenerService;

	@Value("${urlshortener.url.scheme:http}")
	private String urlScheme;
	/**
	 * Will return shortUrl context for given longUrl
	 * @param longUrl - a long url string e.g. https://www.github.com
	 * @return ShortenerBean A json with ShortUrl e.g. http://localhost:8080/Zh8LFilehX
	 */
	@RequestMapping(value="/shortenerservice/shorten", 
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE)
    public ShortenerBean shortenUrl(
    		@RequestBody String longUrl,
    		@RequestHeader HttpHeaders headers) {

		logger.info("Incoming longUrl " + longUrl);
		ShortenerBean shortenerBean = shortenerService.shorten(longUrl);
		fillShortUrl(headers, shortenerBean);
		return shortenerBean;
    }

	private void fillShortUrl(HttpHeaders headers, ShortenerBean shortenerBean) {
		InetSocketAddress host = headers.getHost();
		if (host == null) {
			return;
		}
		String shortUrl = urlScheme + "://" + host.getHostName();
		if (host.getPort() > 0) {
			shortUrl += ":" + host.getPort();
		}
		shortUrl += "/" + shortenerBean.getShortUrlContext();
		shortenerBean.setShortUrl(shortUrl);
	}

	@RequestMapping(value="/{shortUrlContext:[a-zA-Z0-9]+}", 
			method = RequestMethod.GET)
    public void gotoUrl(
    		@PathVariable("shortUrlContext") String shortUrlContext,
    		HttpServletResponse httpServletResponse) throws IOException {

		String longUrl = shortenerService.lookupLongUrl(shortUrlContext);
		if (StringUtils.isEmpty(longUrl)) {
			String message = "shortUrl [ " + shortUrlContext +" ] is unknown";
			logger.error(message);
			throw new RuntimeException(message);
		}
		else {
			httpServletResponse.setHeader("Location", longUrl);
			logger.info("Incoming Shorturl " + shortUrlContext + " Redirecting to " + longUrl);
		    httpServletResponse.sendRedirect(longUrl);
		}
	}
}

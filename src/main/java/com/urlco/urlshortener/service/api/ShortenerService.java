package com.urlco.urlshortener.service.api;

import com.urlco.urlshortener.dto.ShortenerBean;

public interface ShortenerService {
	
	public ShortenerBean shorten(String longUrl);

	public String lookupLongUrl(String shortUrl);
}

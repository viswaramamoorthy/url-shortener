package com.urlco.urlshortener.service.impl;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.urlco.urlshortener.db.api.KeyValueStore;
import com.urlco.urlshortener.dto.ShortenerBean;
import com.urlco.urlshortener.service.api.ShortenerService;
import com.urlco.urlshortener.util.ShortenerConstants;
import com.urlco.urlshortener.util.ShortenerUrlValidator;

@Component
public class ShortenerServiceImpl implements ShortenerService {

	private Log logger = LogFactory.getLog(ShortenerServiceImpl.class);

	@Value("${urlshortener.short.url.context.length:10}")
	private int shortUrlContextLength;

	@Autowired
	private KeyValueStore keyValueStore;

	@Autowired
	private ShortenerUrlValidator shotenerUrlValidator;

	@Override
	public ShortenerBean shorten(String longUrl) {
		ShortenerBean shortenerBean = new ShortenerBean();
		if (!shotenerUrlValidator.validate(longUrl)) {
			shortenerBean.setStatusCode(ShortenerConstants.Status.ERROR.name());
			shortenerBean.setStatusMessage(ShortenerConstants.INVALID_URL);
		}
		else {
			String shortUrlContext = keyValueStore.getByValue(longUrl);
			if (StringUtils.isEmpty(shortUrlContext)) {
				logger.info("Url[ " + longUrl + "] not seen so far; generating short url");
				shortUrlContext = generateRandom();
				keyValueStore.put(shortUrlContext, longUrl);
			}
			else {
				logger.info("Url[ " + longUrl + "] already known using existing "
						+ "shortUrl [" + shortUrlContext+"]");
			}
			shortenerBean.setStatusCode(ShortenerConstants.Status.SUCCESS.name());
			shortenerBean.setShortUrlContext(shortUrlContext);
		}
		return shortenerBean;
	}

	@Override
	public String lookupLongUrl(String shortUrl) {
		String longUrl = keyValueStore.get(shortUrl);
		return longUrl;
	}

	private String generateRandom() {
		return RandomStringUtils.random(shortUrlContextLength, true, true);
	}
}

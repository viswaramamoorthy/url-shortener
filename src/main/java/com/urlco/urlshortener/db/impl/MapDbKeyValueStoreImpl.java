package com.urlco.urlshortener.db.impl;

import java.io.File;
import java.util.Map.Entry;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import com.urlco.urlshortener.db.api.KeyValueStore;

@Component
@ConditionalOnProperty(prefix = "urlshortener", name = "db.type", havingValue="mapdb", matchIfMissing=true)
public class MapDbKeyValueStoreImpl implements KeyValueStore, InitializingBean {

	private Log logger = LogFactory.getLog(MapDbKeyValueStoreImpl.class);
	
	@Value("${urlshortener.mapdb.path:#{systemProperties['java.io.tmpdir']}}")
	private String mapDBPath;
	private static final String URL_MAP_FILE = "urlmap.db";

	private DB diskStore = null;
	private HTreeMap<String, String> urlDBMap = null;

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Using path " + mapDBPath + " to store map db. "
				+ "To change location, set env varaible urlshortener.mapdb.path");

		diskStore = DBMaker
		        .fileDB(mapDBPath + File.separator + URL_MAP_FILE)
		        .transactionEnable()
		        .concurrencyScale(5)
		        .closeOnJvmShutdown()
		        .make();

		urlDBMap = diskStore
		        .hashMap("urlDBMap", Serializer.STRING, Serializer.STRING)
		        .createOrOpen();
	}

	@Override
	public void put(String key, String value) {
		urlDBMap.put(key, value);
		diskStore.commit();
	}

	@Override
	public String get(String key) {
		return urlDBMap.get(key);
	}

	@Override
	public String getByValue(String value) {
		String key = null;
		for (Entry<String, String> entry : urlDBMap.entrySet()) {
			if (entry.getValue().equals(value)) {
				key = entry.getKey();
				break;
			}
		}
		return key;
	}

	@Override
	public void delete(String key) {
		urlDBMap.remove(key);
		diskStore.commit();
	}
}

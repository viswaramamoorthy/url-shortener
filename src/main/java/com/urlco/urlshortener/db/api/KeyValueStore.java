package com.urlco.urlshortener.db.api;

public interface KeyValueStore {
	
	public void put(String key, String value);

	public String get(String key);

	public String getByValue(String value);

	public void delete(String key);
}

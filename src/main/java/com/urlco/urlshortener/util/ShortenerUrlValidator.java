package com.urlco.urlshortener.util;

import org.apache.commons.validator.routines.UrlValidator;
import org.springframework.stereotype.Component;

@Component
public class ShortenerUrlValidator {
    private String[] schemes = {"http","https"};
    private UrlValidator urlValidator = new UrlValidator(schemes);

    public boolean validate(String url) {
    	return urlValidator.isValid(url);
    }
}

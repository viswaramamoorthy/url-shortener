package com.urlco.urlshortener.util;

public class ShortenerConstants {
	
	public enum Status {
		ERROR,
		SUCCESS
	}

	public static final String INVALID_URL="Invalid Url. Cannot shorten";
	public static final String UNKNOWN_SHORT_URL="Short Url not found";
}

package com.urlco.urlshortener.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ShortenerBean {
	@JsonProperty("StatusCode")
	private String statusCode;

	@JsonProperty("StatusMessage")
	private String statusMessage;

	@JsonProperty("LongUrl")
	private String longUrl;

	@JsonProperty("ShortUrlContext")
	private String shortUrlContext;

	@JsonProperty("ShortUrl")
	private String shortUrl;

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}

	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}

	public String getLongUrl() {
		return longUrl;
	}

	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}

	public String getShortUrlContext() {
		return shortUrlContext;
	}

	public void setShortUrlContext(String shortUrlContext) {
		this.shortUrlContext = shortUrlContext;
	}

	public String getShortUrl() {
		return shortUrl;
	}

	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
}

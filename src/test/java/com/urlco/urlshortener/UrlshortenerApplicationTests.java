package com.urlco.urlshortener;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.util.NestedServletException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.urlco.urlshortener.db.api.KeyValueStore;
import com.urlco.urlshortener.dto.ShortenerBean;
import com.urlco.urlshortener.service.api.ShortenerService;

@SpringBootTest(webEnvironment=WebEnvironment.MOCK)
@TestInstance(Lifecycle.PER_CLASS)
public class UrlshortenerApplicationTests {

	private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;	

    @Autowired
	private ShortenerService shortenerService;

	@Autowired
	private KeyValueStore keyValueStore;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@BeforeAll
    void setUp() {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
    }

	@Test
	void testShorten() {
		ShortenerBean sBean = shortenerService.shorten("test");
		Assertions.assertTrue(sBean != null, "Expected not null shorten results");
		Assertions.assertTrue(sBean.getStatusCode().equals("ERROR"), "Expected ERROR status code");
		
		String longUrl = "http://www.test.com";
		sBean = shortenerService.shorten(longUrl);
		Assertions.assertTrue(sBean != null, "Expected not null shorten results");
		Assertions.assertTrue(sBean.getStatusCode().equals("SUCCESS"), 
				"Expected ERROR status code");
		String shortUrl = sBean.getShortUrlContext();
		Assertions.assertTrue(StringUtils.isNotEmpty(shortUrl), 
				"Expected not null shortUrl");
		String fetchedLongUrl = shortenerService.lookupLongUrl(shortUrl);
		Assertions.assertTrue(longUrl.equals(fetchedLongUrl), 
				"Expected [ " + longUrl + "] LongUrl "
						+ "but got [ "+ fetchedLongUrl +"]");

		//When called second time, expected same ShortUrl fetched from key store
		sBean = shortenerService.shorten(longUrl);
		Assertions.assertTrue(sBean != null, "Expected not null shorten results");
		Assertions.assertTrue(sBean.getStatusCode().equals("SUCCESS"), 
				"Expected ERROR status code");
		Assertions.assertTrue(StringUtils.isNotEmpty(sBean.getShortUrlContext()), 
				"Expected not null shortUrl");
		Assertions.assertTrue(sBean.getShortUrlContext().equals(shortUrl), 
				"Expected [ " + shortUrl + "] shortUrl "
						+ "but got [ "+ sBean.getShortUrlContext() +"]");

		String fetchByValue = keyValueStore.getByValue(longUrl);
		Assertions.assertTrue(fetchByValue.equals(shortUrl), 
				"Expected [ " + shortUrl + "] shortUrl "
						+ "but got [ "+ fetchByValue +"]");
		//Cleanup
		keyValueStore.delete(sBean.getShortUrlContext());
	}

	@Test
	void testRestShorten() throws Exception {
		String longUrl = "http://www.test.com";
		ResultActions actions = mockMvc.perform(post("/shortenerservice/shorten")
				   .header("Host", "localhost:8080")
		           .contentType(MediaType.TEXT_PLAIN)
		           .content(longUrl) 
		           .accept(MediaType.APPLICATION_JSON))
		           .andExpect(status().isOk())
		           .andExpect(content().contentType(MediaType.APPLICATION_JSON))
		           .andExpect(jsonPath("$.StatusCode").value("SUCCESS"));
		
		MvcResult result = actions.andReturn();
		
		String responseContent = result.getResponse().getContentAsString();
		ShortenerBean sBean = objectMapper.readValue(responseContent, ShortenerBean.class);
		
		String shortUrl = sBean.getShortUrlContext();
		Assertions.assertTrue(StringUtils.isNotEmpty(shortUrl), 
				"Expected not null shortUrl");
		String lookupUri = "/" + shortUrl;
		actions = mockMvc.perform(get(lookupUri))
		           .andExpect(status().is3xxRedirection());
		//Cleanup
		keyValueStore.delete(sBean.getShortUrlContext());
	}

	@Test
	void testRestShortenExpectException() throws Exception {
		String lookupUri = "/unknown";
		Throwable t = Assertions.assertThrows(NestedServletException.class, () -> {
			mockMvc.perform(get(lookupUri));
		});
		Assertions.assertTrue(t.getMessage().contains("shortUrl [ unknown ] is unknown"), 
				"Expected exception not thrown");
	}
}
